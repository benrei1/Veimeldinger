package com.example.benrei.veimeldinger.Listeners

import com.example.benrei.veimeldinger.DTO.Item
import com.example.benrei.veimeldinger.Domain.Trafikkmelding

/**
 * Created by benrei on 15.01.2018.
 */
interface AdapterListener {
    fun onRowClicked(item: Trafikkmelding)
}