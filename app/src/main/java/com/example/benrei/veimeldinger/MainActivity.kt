package com.example.benrei.veimeldinger

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.benrei.veimeldinger.Fragments.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.io.IOException

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    var searchView: SearchView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        handleIntent(intent)
    }

    override fun onNewIntent(intent: Intent) {
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {

        if (Intent.ACTION_SEARCH == intent.action) {
            val query = intent.getStringExtra(SearchManager.QUERY)
            //use the query to search your data somehow
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.search)
                .actionView as SearchView
        searchView!!.setSearchableInfo(searchManager
                .getSearchableInfo(componentName))
        searchView!!.setMaxWidth(Integer.MAX_VALUE)

        if(isConnected()){
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=600"))
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
            toolbar.title = "Viktige Trafikkmeldinger"
        }


        return true
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        if(isConnected()){
            when (item.itemId) {
                R.id.nav_vt -> {
                    // Handle the camera action
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=600"))
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Viktige Trafikkmeldinger"
                }
                R.id.nav_sk -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=601"))
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Stengninger og kolonnekjøring"
                }
                R.id.nav_rf -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=602"))
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Redusert framkommelighet"
                }
                R.id.nav_fs -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=604"))
                    fragmentTransaction.commit()
                    toolbar.title = "Fjelloverganger i  Sør-Norge"
                }
                R.id.nav_fn -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VeimeldingerFragment(searchView!!,"https://www.vegvesen.no/trafikk/xml/savedsearch.rss?id=605"))
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Fjelloverganger i  Nord-Norge"
                }
                R.id.nav_outOslo -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, HovedveierUtOsloFragment())
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Hovedveger ut fra Oslo"
                }
                R.id.nav_inOslo -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, HovedveierInnOsloFragment())
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Hovedveger inn mot Oslo"
                }
                R.id.nav_veinr -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, VegnummerFragment())
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Vegnummer"
                }
                R.id.nav_fylke -> {
                    val fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.replace(R.id.fragment_container, FylkeFragment())
                    fragmentTransaction.addToBackStack(null)
                    fragmentTransaction.commit()
                    toolbar.title = "Fylke"
                }
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    @Throws(InterruptedException::class, IOException::class)
    fun isConnected(): Boolean {
        val command = "ping -c 1 google.com"
        var bool = Runtime.getRuntime().exec(command).waitFor() == 0
        if (!bool) Toast.makeText(applicationContext, "Ikke koblet til internett", Toast.LENGTH_LONG).show()
        return bool
    }

}
