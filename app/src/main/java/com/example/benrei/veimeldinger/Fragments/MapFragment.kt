package com.example.benrei.veimeldinger.Fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.benrei.veimeldinger.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker








@SuppressLint("ValidFragment")
/**
 * A simple [Fragment] subclass.
 */
class MapFragment(val coordinate1: LatLng?, val coordinate2: LatLng?) : Fragment() {

    lateinit var mMapView: MapView
    private var googleMap: GoogleMap? = null

    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater!!.inflate(R.layout.fragment_map, container, false)

        mMapView = v.findViewById(R.id.mapView)
        mMapView.onCreate(savedInstanceState)

        mMapView.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(activity.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mMapView.getMapAsync { mMap ->
            googleMap = mMap
            val builder = LatLngBounds.Builder()

            // For showing a move to my location button
            //googleMap!!.isMyLocationEnabled = true

            // For dropping a marker at a point on the Map
            val sydney = LatLng(-34.0, 151.0)
            coordinate1?.let {
                val markerOptions = MarkerOptions().position(it).title("Marker Title").snippet("Marker Description")
                builder.include(markerOptions.position)
                mMap.addMarker(markerOptions)
            }
            coordinate2?.let {
                val markerOptions = MarkerOptions().position(it).title("Marker Title").snippet("Marker Description")
                builder.include(markerOptions.position)
                mMap.addMarker(markerOptions)
            }

            // For zooming automatically to the location of the marker
            if(coordinate2== null){
                val cameraPosition = CameraPosition.Builder().target(coordinate1).zoom(12f).build()
                googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }else{
                val bounds = builder.build()
                googleMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            }

        }

        return v
    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }

}// Required empty public constructor
