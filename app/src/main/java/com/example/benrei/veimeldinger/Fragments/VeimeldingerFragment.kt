package com.example.benrei.veimeldinger.Fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.benrei.veimeldinger.Async_task.MyAsyncTask
import com.example.benrei.veimeldinger.DTO.DTO
import com.example.benrei.veimeldinger.Domain.Trafikkmelding
import com.example.benrei.veimeldinger.Listeners.AdapterListener
import com.example.benrei.veimeldinger.Mappers.TrafikkmeldingMapper

import com.example.benrei.veimeldinger.R
import com.example.benrei.veimeldinger.Utils
import com.example.benrei.veimeldinger.VeimeldingerAdapter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import java.util.ArrayList


@SuppressLint("ValidFragment")
/**
 * A simple [Fragment] subclass.
 */
class VeimeldingerFragment(val searchView: SearchView, val url: String) : Fragment(), OnMapReadyCallback, AdapterListener {

    private var veimeldingerAdapter: VeimeldingerAdapter? = null
    private var trafikkmeldingerList: MutableList<Trafikkmelding>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater!!.inflate(R.layout.fragment_veimeldinger, container, false)

        trafikkmeldingerList = ArrayList()
        veimeldingerAdapter = trafikkmeldingerList?.let { VeimeldingerAdapter(trafikkmeldingerList as ArrayList<Trafikkmelding>, this) }

        val recyclerView = v.findViewById<RecyclerView>(R.id.recycle_view)
        recyclerView.setHasFixedSize(true)
        recyclerView!!.layoutManager = LinearLayoutManager(context)
        recyclerView!!.adapter = veimeldingerAdapter
        //  Get data
        FetchData().execute(url)

        // Link SearchView to Adapter
        Utils.Attach.SearchViewToAdapter(searchView, veimeldingerAdapter!!)

        return v
    }
    override fun onMapReady(p0: GoogleMap?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onRowClicked(item: Trafikkmelding) {
        if(item.latLngStart != null){
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.fragment_container, MapFragment(item.latLngStart,item.latLngEnd))
            fragmentTransaction.addToBackStack(null)
            fragmentTransaction.commit()
        }else{
            Toast.makeText(this.context, "Ingen koordinater å vise ", Toast.LENGTH_LONG).show()
        }
    }

    internal inner class FetchData : MyAsyncTask() {
        override protected fun onPreExecute() {
            super.onPreExecute()
        }

        override protected fun onPostExecute(dataModels: DTO) {
            super.onPostExecute(dataModels)

            val trafikkmeldinger = TrafikkmeldingMapper.MapTrafikkmeldinger(dataModels)
            trafikkmeldingerList!!.clear()
            trafikkmeldingerList!!.addAll(trafikkmeldinger)
            veimeldingerAdapter!!.notifyDataSetChanged()
        }

    }
}// Required empty public constructor