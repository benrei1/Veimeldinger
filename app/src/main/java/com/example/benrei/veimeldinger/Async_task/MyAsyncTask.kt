package com.example.benrei.veimeldinger.Async_task

import android.os.AsyncTask
import com.example.benrei.veimeldinger.DTO.DTO
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import fr.arnaudguyon.xmltojsonlib.XmlToJson
import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Created by benrei on 02.01.2018.
 */

internal abstract class MyAsyncTask : AsyncTask<String, String, DTO>() {
    override fun doInBackground(vararg params: String): DTO {

        val client = OkHttpClient()
        val request = Request.Builder().url(params[0]).build()
        val response = client.newCall(request).execute()

        var xml = response.body()?.string()
        xml = xml!!.replace("(georss:where)".toRegex(),"georss")
        xml = xml!!.replace("(gml:Point)".toRegex(),"itemPoints")
        xml = xml!!.replace("(gml:pos)".toRegex(),"pointLatLng")


        var jsonFromXml = xml?.let {
            XmlToJson.Builder(it).build().toString()
        }
        var sText = "\"itemPoints\":{"

        var startIndex = jsonFromXml.indexOf(sText)
        while (startIndex > 0){

            var indexLast = jsonFromXml.indexOf('}', startIndex)
            var oldStr = jsonFromXml.substring(startIndex, indexLast+1)
            var newStr = StringBuilder(oldStr)
            var indexFirst = newStr.indexOf('{')
            newStr.insert(indexFirst,"[")
            jsonFromXml = jsonFromXml.replace(oldStr, newStr.toString()+"]")

            startIndex = jsonFromXml.indexOf(sText)
        }

        val objectMapper = ObjectMapper()
        objectMapper.registerModule(KotlinModule())
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        val obj = objectMapper.readValue(jsonFromXml, DTO::class.java)

        return obj
    }


}