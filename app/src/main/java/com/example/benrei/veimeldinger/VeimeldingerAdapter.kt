package com.example.benrei.veimeldinger

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import com.example.benrei.veimeldinger.Domain.Trafikkmelding
import com.example.benrei.veimeldinger.Listeners.AdapterListener

/**
 * Created by benrei on 16.01.2018.
 */
class VeimeldingerAdapter(private val list: List<Trafikkmelding>, private val listener: AdapterListener) : RecyclerView.Adapter<VeimeldingerAdapter.MyViewHolder>(), Filterable {
    private var listFiltered: List<Trafikkmelding>? = null

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var description: TextView
        var linearLayout: LinearLayout

        init {
            title = view.findViewById(R.id.title)
            description = view.findViewById(R.id.description)
            linearLayout = view.findViewById(R.id.linearLayout)
        }
    }

    init {
        this.listFiltered = list
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.recycleview, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = listFiltered!![position]

        holder.title.text = RemoveBackSlashesFromBeginningOfString(item.title.toString())
        holder.description.text = RemoveBackSlashesFromBeginningOfString(item.description.toString())
        holder.linearLayout.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                listener.onRowClicked(item)
            }

        })


    }

    override fun getItemCount(): Int {
        return listFiltered!!.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): Filter.FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    listFiltered = list
                } else {
                    val filteredList = ArrayList<Trafikkmelding>()
                    for (row in list) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.title!!.toLowerCase().contains(charString.toLowerCase()) || row.description!!.toLowerCase().contains(charSequence)) {
                            filteredList.add(row)
                        }
                    }

                    listFiltered = filteredList
                }

                val filterResults = Filter.FilterResults()
                filterResults.values = listFiltered
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
                listFiltered = filterResults.values as ArrayList<Trafikkmelding>
                notifyDataSetChanged()
            }
        }
    }


    fun RemoveBackSlashesFromBeginningOfString(str:String): String {
        var myStr = str
        while (myStr.get(0).toString() == "\n" || myStr.get(0).toString() == "\t"){
            myStr = myStr.substring(1)
        }
        return myStr
    }
}