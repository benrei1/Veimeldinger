package com.example.benrei.veimeldinger

import android.support.v7.widget.SearchView

/**
 * Created by benrei on 02.01.2018.
 */
class Utils {

    internal object Attach {
        fun SearchViewToAdapter (searchView: SearchView, adapter: VeimeldingerAdapter){
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    // filter recycler view when query submitted
                    adapter!!.filter.filter(query)
                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    // filter recycler view when text is changed
                    adapter!!.filter.filter(query)
                    return false
                }
            })
        }
    }

}