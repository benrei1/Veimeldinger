package com.example.benrei.veimeldinger.Mappers

import com.example.benrei.veimeldinger.DTO.DTO
import com.example.benrei.veimeldinger.Domain.Trafikkmelding
import com.google.android.gms.maps.model.LatLng

/**
 * Created by benrei on 15.01.2018.
 */
class TrafikkmeldingMapper {
    companion object {
        fun MapTrafikkmeldinger(obj:DTO):List<Trafikkmelding>{

            var item = obj.rss!!.channel!!.item

            val trafikkmeldinger = mutableListOf<Trafikkmelding>()

            item!!.forEach { item->
                val title = item.title
                val description = item.description
                var latLngStart : LatLng? = null
                var latLngEnd : LatLng? = null

                item.georss?.itemPoints?.let {
                    if(it.size == 1 || it.size == 2) latLngStart = GetLatLngFromString(item.georss!!.itemPoints!![0].pointLatLng)
                    if(it.size == 2) latLngEnd = GetLatLngFromString(item.georss!!.itemPoints!![1].pointLatLng)
                }

                trafikkmeldinger.add(Trafikkmelding(description,title,latLngStart,latLngEnd))
            }
            return trafikkmeldinger
        }

        private fun GetLatLngFromString(str: String): LatLng?{
            str.let {
                var points = it.split(" ")
                return LatLng(points[0].toDouble(), points[1].toDouble()) }

        }
    }


}