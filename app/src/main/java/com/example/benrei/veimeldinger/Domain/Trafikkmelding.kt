package com.example.benrei.veimeldinger.Domain

import com.google.android.gms.maps.model.LatLng

/**
 * Created by benrei on 15.01.2018.
 */
class Trafikkmelding(
        val description: String,
        val title: String,
        val latLngStart: LatLng?,
        val latLngEnd: LatLng?
)