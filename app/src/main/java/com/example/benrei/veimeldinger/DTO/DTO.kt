package com.example.benrei.veimeldinger.DTO

data class Item(val georss: Georss?,
                    val description: String = "",
                    val title: String = "")


data class Rss(val channel: Channel?
               )

data class Channel(val description: String = "",
                   val title: String = "",
                   val item: List<Item>?
                   )

data class Georss(val itemPoints: List<ItemPoint>?)


data class ItemPoint(val pointLatLng: String = "")


data class DTO(val rss: Rss?)


